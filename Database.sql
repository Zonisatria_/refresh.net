USE [master]
GO
/****** Object:  Database [applicant_db]    Script Date: 7/8/2020 4:12:12 PM ******/
CREATE DATABASE [applicant_db]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'applicant_db', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\applicant_db.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'applicant_db_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\applicant_db_log.ldf' , SIZE = 560KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [applicant_db] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [applicant_db].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [applicant_db] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [applicant_db] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [applicant_db] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [applicant_db] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [applicant_db] SET ARITHABORT OFF 
GO
ALTER DATABASE [applicant_db] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [applicant_db] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [applicant_db] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [applicant_db] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [applicant_db] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [applicant_db] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [applicant_db] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [applicant_db] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [applicant_db] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [applicant_db] SET  ENABLE_BROKER 
GO
ALTER DATABASE [applicant_db] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [applicant_db] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [applicant_db] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [applicant_db] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [applicant_db] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [applicant_db] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [applicant_db] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [applicant_db] SET RECOVERY FULL 
GO
ALTER DATABASE [applicant_db] SET  MULTI_USER 
GO
ALTER DATABASE [applicant_db] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [applicant_db] SET DB_CHAINING OFF 
GO
ALTER DATABASE [applicant_db] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [applicant_db] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [applicant_db] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'applicant_db', N'ON'
GO
USE [applicant_db]
GO
/****** Object:  Table [dbo].[tb_jurusan]    Script Date: 7/8/2020 4:12:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_jurusan](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
	[nama] [varchar](50) NULL,
	[ket] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_pelamar]    Script Date: 7/8/2020 4:12:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_pelamar](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
	[nama] [varchar](50) NOT NULL,
	[no_hp] [varchar](12) NOT NULL,
	[email] [varchar](30) NULL,
	[tanggal_lahir] [datetime] NOT NULL,
	[alamat] [varchar](100) NOT NULL,
	[jurusan] [bigint] NULL,
	[pertanyaan] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tb_jurusan] ON 

INSERT [dbo].[tb_jurusan] ([id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete], [nama], [ket]) VALUES (1, 1, CAST(N'2020-07-07 00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0, N'IT', NULL)
INSERT [dbo].[tb_jurusan] ([id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete], [nama], [ket]) VALUES (2, 1, CAST(N'2020-07-07 00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0, N'Non IT', NULL)
SET IDENTITY_INSERT [dbo].[tb_jurusan] OFF
SET IDENTITY_INSERT [dbo].[tb_pelamar] ON 

INSERT [dbo].[tb_pelamar] ([id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete], [nama], [no_hp], [email], [tanggal_lahir], [alamat], [jurusan], [pertanyaan]) VALUES (1, 1, CAST(N'2020-07-07 00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0, N'Zoni Satria', N'082280602742', N'zonisatri@gmail.com', CAST(N'1994-11-10 00:00:00.000' AS DateTime), N'Jambi', 2, NULL)
INSERT [dbo].[tb_pelamar] ([id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete], [nama], [no_hp], [email], [tanggal_lahir], [alamat], [jurusan], [pertanyaan]) VALUES (2, 1, CAST(N'2020-07-08 11:36:25.697' AS DateTime), NULL, NULL, NULL, NULL, 0, N'Bagas Slimbos', N'082297652465', N'bagas@gmail.com', CAST(N'1986-06-16 00:00:00.000' AS DateTime), N'Papua', 2, NULL)
INSERT [dbo].[tb_pelamar] ([id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete], [nama], [no_hp], [email], [tanggal_lahir], [alamat], [jurusan], [pertanyaan]) VALUES (3, 1, CAST(N'2020-07-08 13:08:00.940' AS DateTime), NULL, NULL, 1, CAST(N'2020-07-08 13:45:51.977' AS DateTime), 1, N'Inas Eifel', N'082265771200', N'ieif@gmail.com', CAST(N'1998-06-09 00:00:00.000' AS DateTime), N'Jakarta', 1, N'Hayoo apa ?')
INSERT [dbo].[tb_pelamar] ([id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete], [nama], [no_hp], [email], [tanggal_lahir], [alamat], [jurusan], [pertanyaan]) VALUES (4, 1, CAST(N'2020-07-08 13:08:00.940' AS DateTime), 1, CAST(N'2020-07-08 13:45:33.570' AS DateTime), NULL, NULL, 0, N'Inas Eifel Emco', N'082265771200', N'ieif@gmail.com', CAST(N'1998-06-09 00:00:00.000' AS DateTime), N'Jakarta', 1, N'Hayoo apa ?')
INSERT [dbo].[tb_pelamar] ([id], [created_by], [created_on], [modified_by], [modified_on], [deleted_by], [deleted_on], [is_delete], [nama], [no_hp], [email], [tanggal_lahir], [alamat], [jurusan], [pertanyaan]) VALUES (5, 1, CAST(N'2020-07-08 13:33:15.427' AS DateTime), NULL, NULL, NULL, NULL, 0, N'Zoni Satria', N'082280602742', N'zoni@gmail.com', CAST(N'2006-03-01 00:00:00.000' AS DateTime), N'Padang', 2, N'Test ?')
SET IDENTITY_INSERT [dbo].[tb_pelamar] OFF
USE [master]
GO
ALTER DATABASE [applicant_db] SET  READ_WRITE 
GO
