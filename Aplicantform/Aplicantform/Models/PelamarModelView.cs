﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aplicantform.Models
{
    public class PelamarModelView
    {
        public long id { get; set; }
        public long created_by { get; set; }
        public System.DateTime created_on { get; set; }
        public Nullable<long> modified_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
        public Nullable<long> deleted_by { get; set; }
        public Nullable<System.DateTime> deleted_on { get; set; }
        public bool is_delete { get; set; }
        public string nama { get; set; }
        public string no_hp { get; set; }
        public string email { get; set; }
        public System.DateTime tanggal_lahir { get; set; }
        public string tanggal => tanggal_lahir.ToString("dd MMMM yyyy");
        public string alamat { get; set; }
        public Nullable<long> jurusan { get; set; }
        public string jurusan_show { get; set; }
        public string pertanyaan { get; set; }
    }
}